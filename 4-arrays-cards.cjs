/*const data = [{ "id": 1, "card_number": "5602221055053843723", "card_type": "china-unionpay", "issue_date": "5/25/2021", "salt": "x6ZHoS0t9vIU", "phone": "339-555-5239" },
{ "id": 2, "card_number": "3547469136425635", "card_type": "jcb", "issue_date": "12/18/2021", "salt": "FVOUIk", "phone": "847-313-1289" },
{ "id": 3, "card_number": "5610480363247475108", "card_type": "china-unionpay", "issue_date": "5/7/2021", "salt": "jBQThr", "phone": "348-326-7873" },
{ "id": 4, "card_number": "374283660946674", "card_type": "americanexpress", "issue_date": "1/13/2021", "salt": "n25JXsxzYr", "phone": "599-331-8099" },
{ "id": 5, "card_number": "67090853951061268", "card_type": "laser", "issue_date": "3/18/2021", "salt": "Yy5rjSJw", "phone": "850-191-9906" },
{ "id": 6, "card_number": "560221984712769463", "card_type": "china-unionpay", "issue_date": "6/29/2021", "salt": "VyyrJbUhV60", "phone": "683-417-5044" },
{ "id": 7, "card_number": "3589433562357794", "card_type": "jcb", "issue_date": "11/16/2021", "salt": "9M3zon", "phone": "634-798-7829" },
{ "id": 8, "card_number": "5602255897698404", "card_type": "china-unionpay", "issue_date": "1/1/2021", "salt": "YIMQMW", "phone": "228-796-2347" },
{ "id": 9, "card_number": "3534352248361143", "card_type": "jcb", "issue_date": "4/28/2021", "salt": "zj8NhPuUe4I", "phone": "228-796-2347" },
{ "id": 10, "card_number": "4026933464803521", "card_type": "visa-electron", "issue_date": "10/1/2021", "salt": "cAsGiHMFTPU", "phone": "372-887-5974" }]
*/


const enumMonth= {1:"Jan" , 2:"Feb", 3:"Mar", 4:"Apr", 
                5:"May", 6:"June", 7:"July", 8:"Aug", 
                9:"Sep", 10:"Oct", 11:"Nov" ,12:"Dec" };

// 1. Find all card numbers whose sum of all the even position digits is odd.

function cardnumbersWithSumOfEvenPositionOdd(cards) {
    return cards.filter((card) => {
        let sum = 0;

        for (let i = 0; i < card.card_number.length; i++) {
            if (i % 2 == 0)
                sum += parseInt(card.card_number.charAt(i));
        }
        
        if(sum%2==1)
            return true;
        else
            return false;
    });
}

//console.log(cardnumbersWithSumOfEvenPositionOdd(data));

//2. Find all cards that were issued before June.
function issuedBeforeJune(cards)
{
    return cards.filter((card)=>{
        if(parseInt(card.issue_date.slice(0,2))<7)
            return true;
        else
            return false;
    })
}

//console.log(issuedBeforeJune(data));


function chaining(cards)
{
    return cards.map((card)=>{
        card.CVV = Math.floor(Math.random() * 1000 + 1); 
        return card; //3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    }).map((card)=>{
        card.valid = true;
        return card; //4. Add a new field to each card to indicate if the card is valid or not.
    }).map((card)=>{
        if(parseInt(card.issue_date.slice(0,2))<3)
            card.valid = false;
        return card; //5. Invalidate all cards issued before March.
    }).sort(function(card1, card2){
        let card1Arr = card1.issue_date.split('/');
        let card2Arr = card2.issue_date.split('/');
        if(parseInt(card1Arr[2])>parseInt(card2Arr[2]))
            return 1;
        else if(parseInt(card1Arr[2])<parseInt(card2Arr[2]))
            return -1;
        else
        {
            if(parseInt(card1Arr[0])>parseInt(card2Arr[0]))
                return 1;
            else if(parseInt(card1Arr[0])<parseInt(card2Arr[0]))
                return -1;
            else
            {
                if(parseInt(card1Arr[1])>parseInt(card2Arr[1]))
                    return 1;
                else if(parseInt(card1Arr[1])<parseInt(card2Arr[1]))
                    return -1;
                else
                    return 0;
            }
        }//6. Sort the data into ascending order of issue date.
    }).reduce((acc,card)=>{
        if(acc.hasOwnProperty(enumMonth[parseInt(card.issue_date.slice(0,2))]))
        {
            acc[enumMonth[parseInt(card.issue_date.slice(0,2))]].push(card);
        }
        else
        {
            acc[enumMonth[parseInt(card.issue_date.slice(0,2))]] = [];
            acc[enumMonth[parseInt(card.issue_date.slice(0,2))]].push(card);
        }
        return acc;
    },{});//7. Group the data in such a way that we can identify which cards were assigned in which months.
}

//console.log(chaining(data));
